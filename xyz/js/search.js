'use strict';

this.bind = (index) => {
  const hiddenField = document.getElementById('hidden-field');
  const searchField = document.getElementById('search-field');

  switch (index) {
    case 'baidu':
      searchField.name = 'wd';
      break;
    case 'bing':
      hiddenField.name = 'ensearch';
      hiddenField.value = '1';
      searchField.name = 'q';
      break;
    case 'duckduckgo':
      searchField.name = 'q';
      break;
    case 'google':
      searchField.name = 'q';
      break;
    case 'github':
      searchField.name = 'q';
      break;
    case 'wikipedia':
      hiddenField.name = 'language';
      hiddenField.value = 'en';
      searchField.name = 'search';
      break;
    case 'merriam-webster':
      searchField.name = 's';
      break;
    case 'oxford':
      searchField.name = 'q';
      break;
    case 'collins-thesaurus':
      hiddenField.name = 'dictCode';
      hiddenField.value = 'english-thesaurus';
      searchField.name = 'q';
      break;
    case 'merriam-webster-thesaurus':
      if (searchField.value !== '') {
        const element = document.getElementById('merriam-webster-thesaurus');
        const url = element.formAction + '/' + searchField.value;
        window.open(url, '_blank');
        return false;
      }
      break;
    default:
      console.warn('unimplemented');
  }

  return true;
};
